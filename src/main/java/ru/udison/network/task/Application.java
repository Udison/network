package ru.udison.network.task;

public class Application {
    public static void main(String[] args) {
        NetworkCreator littleNetworkCreator = new LittleNetworkCreator();
        Network network = littleNetworkCreator.createNetwork();

        RouteProvider otherRouteProvider = new OtherRouteProvider(network);
        RouteProvider otherRouteProvider2 = new OtherRouteProvider(network);
        otherRouteProvider.fasterPath(network.getElementById(1), network.getElementById(13));
        otherRouteProvider2.cheaperPath(network.getElementById(11), network.getElementById(3));
    }
}
