package ru.udison.network.task;

public interface RouteProvider {
    void fasterPath(ActiveElement taskFrom, ActiveElement taskTo);
    void cheaperPath(ActiveElement taskFrom, ActiveElement taskTo);
}
