package ru.udison.network.task;

import java.util.ArrayList;
import java.util.List;

public class Network {
    private List<ActiveElement> networkElements = new ArrayList<>();
    private List<ActiveElement> pcs;
    private List<ActiveElement> switches;
    private List<ActiveElement> hubs;
    private List<ActiveElement> routers;
    private List<PassiveElement> cables = new ArrayList<>();

    public void setPcs(List<ActiveElement> pcs) {
        this.pcs = pcs;
        networkElements.addAll(pcs);
    }

    public void setSwitches(List<ActiveElement> switches) {
        this.switches = switches;
        networkElements.addAll(switches);
    }

    public void setHubs(List<ActiveElement> hubs) {
        this.hubs = hubs;
        networkElements.addAll(hubs);
    }

    public void setRouters(List<ActiveElement> routers) {
        this.routers = routers;
        networkElements.addAll(routers);
    }

    public void setCables(List<PassiveElement> cables) {
        this.cables.addAll(cables);
    }

    public List<ActiveElement> getPcs() {
        List<ActiveElement> pcsToGet = new ArrayList<>(pcs);
        return pcsToGet;
    }

    public List<ActiveElement> getSwitches () {
        List<ActiveElement> switchesToGet = new ArrayList<>(switches);
        return switchesToGet;
    }

    public List<ActiveElement> getHubs () {
        List<ActiveElement> hubsToGet = new ArrayList<>(hubs);
        return hubsToGet;
    }

    public List<ActiveElement> getRouters () {
        List<ActiveElement> routersToGet = new ArrayList<>(routers);
        return routersToGet;
    }

    public List<ActiveElement> getNetworkElements () {
        List<ActiveElement> networkElementsToGet = new ArrayList<>(networkElements);
        return networkElementsToGet;
    }

    public ActiveElement getElementById(int id) {
        ActiveElement elementToGet = null;
        for(ActiveElement e : networkElements) {
            if(e.getId() == id) {
                elementToGet = e;
                break;
            }
        }
        return elementToGet;
    }
}

