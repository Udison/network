package ru.udison.network.task;

import java.util.*;

public class Vertex {
     private ActiveElement element;
     private int distance;
     private List<Vertex> vertexPath = new LinkedList<>();
     private Map<ActiveElement, Integer> neighboursToFasterPath;
     private Map<ActiveElement, Integer> neighboursToCheaperPath;

    public Vertex(ActiveElement element) {
        this.element = element;
        setNeighbours(element);
    }

    public ActiveElement getElement() {
        ActiveElement elementToGet = new ActiveElement(element);
        return element;
    }

    public int getDistance() {
        int distanceToGet = distance;
        return distanceToGet;
    }

    public List<Vertex> getVertexPath() {
        List<Vertex> vertexPathToGet = new LinkedList<>(vertexPath);
        return vertexPathToGet;
    }

    public Map<ActiveElement, Integer> getNeighboursToFasterPath() {
        Map<ActiveElement, Integer> neighboursToFasterPathToGet = new HashMap<>(neighboursToFasterPath);
        return neighboursToFasterPathToGet;
    }

    public Map<ActiveElement, Integer> getNeighboursToCheaperPath() {
        Map<ActiveElement, Integer> neighboursToCheaperPathToGet = new HashMap<>(neighboursToCheaperPath);
        return neighboursToCheaperPathToGet;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setVertexPath(List<Vertex> vertexPath) {
        this.vertexPath = vertexPath;
    }
    public void setVertexPath(Vertex vertex) {
        this.vertexPath.add(vertex);
    }

    private void setNeighbours(ActiveElement element) {
        this.neighboursToFasterPath = new HashMap<>();
        this.neighboursToCheaperPath = new HashMap<>();
        for (PassiveElement cable : element.getCables()) {
            if (cable.getPort1().equals(element)) {
                ActiveElement neighbour = cable.getPort2();
                Integer weight = cable.getTimeDelay() + cable.getPort2().getTimeDelay();
                this.neighboursToFasterPath.put(neighbour, weight);
            }
        }
        for (PassiveElement cable : element.getCables()) {
            if (cable.getPort1().equals(element)) {
                ActiveElement neighbour = cable.getPort2();
                Integer weight = cable.getPort2().getCost();
                this.neighboursToCheaperPath.put(neighbour, weight);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return Objects.equals(element, vertex.element);
    }

    @Override
    public int hashCode() {
        return Objects.hash(element);
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "element=" + element +
                '}';
    }
}
