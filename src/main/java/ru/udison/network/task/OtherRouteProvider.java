package ru.udison.network.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OtherRouteProvider implements RouteProvider {
    private Graph graph;

    public OtherRouteProvider(Network network){
        graph = new Graph(network);
    }

    public void cheaperPath(ActiveElement source, ActiveElement destination) {
        for (Vertex v : graph.getVertexList()) {
            v.setDistance(Integer.MAX_VALUE);
        }
        List<Vertex> usedVertexes = new ArrayList<>();
        Vertex currentVertex = null;
        Vertex destinationVertex = null;
        try {
            for (Vertex v : graph.getVertexList()) {
                if (v.getElement().equals(source))
                    currentVertex = v;
                else if (v.getElement().equals(destination))
                    destinationVertex = v;
            }
            currentVertex.setDistance(0);
            usedVertexes.add(currentVertex);
            while (!currentVertex.equals(destinationVertex)) {
                for (Map.Entry<ActiveElement, Integer> entry : currentVertex.getNeighboursToCheaperPath().entrySet()) {
                    for (Vertex v : graph.getVertexList()) {
                        if (v.getElement().equals(entry.getKey())) {
                            if (currentVertex.getDistance() + entry.getValue() < v.getDistance()) {
                                v.setDistance(currentVertex.getDistance() + entry.getValue());
                                v.setVertexPath(currentVertex.getVertexPath());
                                v.setVertexPath(currentVertex);
                            }
                        }
                    }
                }
                usedVertexes.add(currentVertex);
                currentVertex = findMinDistance(usedVertexes);
            }
            String message = "Самый дешёвый путь от эллемента %s до эллемента %s: %s. Стоимость сотавляет: %s центов";
            System.out.println(String.format(message, source.toString(), destination.toString(), currentVertex.getVertexPath().toString(), currentVertex.getDistance()));
        } catch (NullPointerException e) {
            System.out.println("Элемента с данным id не существует");
        }
    }


        public void fasterPath(ActiveElement source, ActiveElement destination) {
            for (Vertex v : graph.getVertexList()) {
                v.setDistance(Integer.MAX_VALUE);

            }
            List<Vertex> usedVertexes = new ArrayList<>();
            Vertex currentVertex = null;
            Vertex destinationVertex = null;
            try {
                for (Vertex v : graph.getVertexList()) {
                    if (v.getElement().equals(source))
                        currentVertex = v;
                    else if (v.getElement().equals(destination))
                        destinationVertex = v;
                }
                currentVertex.setDistance(0);
                usedVertexes.add(currentVertex);
                while (!currentVertex.equals(destinationVertex)) {
                    for (Map.Entry<ActiveElement, Integer> entry : currentVertex.getNeighboursToFasterPath().entrySet()) {
                        for (Vertex v : graph.getVertexList()) {
                            if (v.getElement().equals(entry.getKey())) {
                                if (currentVertex.getDistance() + entry.getValue() < v.getDistance()) {
                                    v.setDistance(currentVertex.getDistance() + entry.getValue());
                                    v.setVertexPath(currentVertex.getVertexPath());
                                    v.setVertexPath(currentVertex);
                                }
                            }
                        }
                    }
                    usedVertexes.add(currentVertex);
                    currentVertex = findMinDistance(usedVertexes);

                }
                String message = "Самый быстрый путь от эллемента %s до эллемента %s: %s. Задержка сотавляет: %s нСек";
                System.out.println(String.format(message, source.toString(), destination.toString(), currentVertex.getVertexPath().toString(), currentVertex.getDistance()));
            } catch (NullPointerException e) {
                System.out.println("Элемента с данным id не существует");
            }
        }

    private Vertex findMinDistance(List<Vertex> usedVertexes) {
        int minDistance = Integer.MAX_VALUE;
        Vertex hasMinDistance = null;
        for (Vertex v : graph.getVertexList()) {
            if (v.getDistance() < minDistance && !usedVertexes.contains(v)) {
                minDistance = v.getDistance();
                hasMinDistance = v;
            }
        }
        return hasMinDistance;
    }
}

