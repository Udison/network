package ru.udison.network.task;

public class PathElement {
    private int timeDelay;


    public PathElement(int timeDelay) {
        this.timeDelay = timeDelay;
    }

    public int getTimeDelay() {
        return timeDelay;
    }
}
