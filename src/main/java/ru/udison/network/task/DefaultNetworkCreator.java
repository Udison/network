package ru.udison.network.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DefaultNetworkCreator implements NetworkCreator {

    public Network createNetwork() {
        Network network = new Network();

        network.setPcs(createPcs());
        network.setSwitches(createSwitches());
        network.setHubs(createHubs());
        network.setRouters(createRouters());
        connectPathElement(network);

        return network;
    }

    public List<ActiveElement> createPcs() {
        List<ActiveElement> pcs = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            int timeDelay = random.nextInt(23);
            int coast = random. nextInt(16);
            pcs.add(new PC(i, timeDelay, coast));
        }
        return pcs;

    }
    public List<ActiveElement> createSwitches() {
        List<ActiveElement> switches = new ArrayList<>();
        Random random = new Random();
        for (int i = 10; i < 12; i++) {
            int timeDelay = random.nextInt(11);
            int coast = random. nextInt(22);
            switches.add(new Switch(i, timeDelay, coast));
        }
        return switches;
    }

    public List<ActiveElement> createHubs() {
        List<ActiveElement> hubs = new ArrayList<>();
        Random random = new Random();
        for (int i = 20; i < 23; i++) {
            int timeDelay = random.nextInt(14);
            int coast = random. nextInt(17);
            hubs.add(new Hub(i, timeDelay, coast));
        }
        return hubs;
    }

    public List<ActiveElement> createRouters() {
        List<ActiveElement> routers = new ArrayList<>();
        Random random = new Random();
        for (int i = 30; i < 33; i++) {
            int timeDelay = random.nextInt(8);
            int coast = random. nextInt(31);
            routers.add(new Router(i, timeDelay, coast));
        }
        return routers;
    }

    public PassiveElement createCable(){
        Random random = new Random();
        int cableTimeDelay = random.nextInt(4);
        return new Cable(cableTimeDelay);
    }
    public void connectPathElement(Network network) {
        List<PassiveElement> cables = new ArrayList<>();
        connectElements(network.getPcs().get(0), network.getPcs().get(1), cables);
        connectElements(network.getPcs().get(0), network.getPcs().get(2), cables);
        connectElements(network.getPcs().get(0), network.getHubs().get(0), cables);
        connectElements(network.getPcs().get(0), network.getHubs().get(1), cables);

        connectElements(network.getPcs().get(1), network.getRouters().get(0), cables);
        connectElements(network.getPcs().get(1), network.getRouters().get(1), cables);

        connectElements(network.getPcs().get(2), network.getPcs().get(3), cables);
        connectElements(network.getPcs().get(2), network.getSwitches().get(0), cables);
        connectElements(network.getPcs().get(2), network.getSwitches().get(1), cables);

        connectElements(network.getPcs().get(3), network.getPcs().get(4), cables);

        connectElements(network.getPcs().get(4), network.getPcs().get(0), cables);

        connectElements(network.getHubs().get(0), network.getPcs().get(3), cables);

        connectElements(network.getHubs().get(1), network.getHubs().get(2), cables);

        connectElements(network.getHubs().get(2), network.getPcs().get(4), cables);

        connectElements(network.getRouters().get(0), network.getRouters().get(2), cables);

        connectElements(network.getRouters().get(1), network.getPcs().get(2), cables);

        connectElements(network.getRouters().get(2), network.getPcs().get(2), cables);

        connectElements(network.getSwitches().get(0), network.getPcs().get(0), cables);

        connectElements(network.getSwitches().get(1), network.getPcs().get(3), cables);

        network.setCables(cables);
    }

    public void connectElements(ActiveElement sourceElement, ActiveElement destinationElement, List<PassiveElement> cables) {
        PassiveElement connectionCable = createCable();
        connectionCable.insertInPort1(sourceElement);
        sourceElement.addCable(connectionCable);
        connectionCable.insertInPort2(destinationElement);
        destinationElement.addCable(connectionCable);
        cables.add(connectionCable);
    }
}
