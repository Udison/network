package ru.udison.network.task;

import java.util.LinkedList;
import java.util.List;

public class Graph {
    List<Vertex> vertexList;

    public Graph(Network network){
        vertexList = new LinkedList<>();
        for (ActiveElement element : network.getNetworkElements()) {
            vertexList.add(new Vertex(element));
        }
    }

    public List<Vertex> getVertexList() {
        List<Vertex> vertexListToGet = new LinkedList<>(vertexList);
        return vertexListToGet;
    }
}
