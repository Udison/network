package ru.udison.network.task;

public interface NetworkCreator {
    Network createNetwork();
}
