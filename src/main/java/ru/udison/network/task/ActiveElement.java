package ru.udison.network.task;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class ActiveElement extends PathElement {


    private int id;
    private int cost;
    private List<PassiveElement> cables = new LinkedList<>();

    public ActiveElement(int id, int timeDelay, int cost) {
        super(timeDelay);
        this.id = id;
        this.cost = cost;
    }

    public ActiveElement(ActiveElement activeElement) {
        super(activeElement.getTimeDelay());
        this.id = activeElement.getId();
        this.cost = activeElement.getCost();
    }

    public int getId() {
        int idToGet = id;
        return idToGet;
    }

    public int getCost() {
        int costToGet = cost;
        return costToGet;
    }

    public List<PassiveElement> getCables() {
        List<PassiveElement> cablesToGet = new ArrayList<>(cables);
        return cablesToGet;
    }

    public void addCable(PassiveElement connectionCable) {
        cables.add(connectionCable);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActiveElement element = (ActiveElement) o;
        return id == element.id &&
                cost == element.cost;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cost, cables);
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "ActiveElement{" +
                "id=" + id +
                '}';
    }
}


