package ru.udison.network.task;

public class PassiveElement extends PathElement {
    private ActiveElement port1;
    private ActiveElement port2;

    public PassiveElement(int timeDelay) {
        super(timeDelay);
    }

    public ActiveElement getPort1() {
        return port1;
    }

    public ActiveElement getPort2() { return port2; }

    public void insertInPort1(ActiveElement sourceElement) {
        port1 = sourceElement;
    }

    public void insertInPort2(ActiveElement destinationElement) {
        port2 = destinationElement;
    }

    public boolean isActive() {
        return false;
    }
}
