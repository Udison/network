package ru.udison.network.task;

import java.util.ArrayList;
import java.util.List;

public class LittleNetworkCreator implements NetworkCreator {
    public Network createNetwork() {
        Network network = new Network();

        network.setPcs(createPcs());
        network.setSwitches(createSwitches());
        network.setHubs(createHubs());
        network.setRouters(createRouters());
        connectPathElement(network);

        return network;
    }

    public List<ActiveElement> createPcs() {
        List<ActiveElement> pcs = new ArrayList<>();
        pcs.add(new PC(1, 11, 9));
        pcs.add(new PC(2, 12, 10));
        pcs.add(new PC(3, 13, 7));
        pcs.add(new PC(4, 10, 8));
        return pcs;

    }
    public List<ActiveElement> createSwitches() {
        List<ActiveElement> switches = new ArrayList<>();
        switches.add(new Switch(11, 2, 17));
        return switches;
    }

    public List<ActiveElement> createHubs() {
        List<ActiveElement> hubs = new ArrayList<>();
        hubs.add(new Hub(12, 8, 10));
        return hubs;
    }

    public List<ActiveElement> createRouters() {
        List<ActiveElement> routers = new ArrayList<>();
        routers.add(new Router(13, 5, 12));
        return routers;
    }

    public PassiveElement createCable(){

        int cableTimeDelay = 2;
        return new Cable(cableTimeDelay);
    }
    public void connectPathElement(Network network) {
        List<PassiveElement> cables = new ArrayList<>();
        connectElements(network.getPcs().get(0), network.getPcs().get(1), cables);
        connectElements(network.getPcs().get(0), network.getSwitches().get(0), cables);
        connectElements(network.getPcs().get(0), network.getHubs().get(0), cables);
        connectElements(network.getPcs().get(0), network.getPcs().get(3), cables);
        connectElements(network.getSwitches().get(0), network.getPcs().get(1), cables);
        connectElements(network.getPcs().get(1), network.getRouters().get(0), cables);
        connectElements(network.getRouters().get(0), network.getPcs().get(2), cables);
        connectElements(network.getHubs().get(0), network.getPcs().get(2), cables);
        connectElements(network.getHubs().get(0), network.getPcs().get(3), cables);
        connectElements(network.getPcs().get(2), network.getPcs().get(3), cables);

        network.setCables(cables);
    }

    public void connectElements(ActiveElement sourceElement, ActiveElement destinationElement, List<PassiveElement> cables) {
        PassiveElement connectionCable = createCable();
        connectionCable.insertInPort1(sourceElement);
        sourceElement.addCable(connectionCable);
        connectionCable.insertInPort2(destinationElement);
        destinationElement.addCable(connectionCable);
        cables.add(connectionCable);
    }
}
