package ru.udison.network.dmka;

import ru.udison.network.dmka.domain.Pluggable;
import ru.udison.network.dmka.domain.Vertex;
import ru.udison.network.task.ActiveElement;

import java.util.*;

public class FastestRouteProvider implements RouteProvider<Long, ActiveElement> {

    @Override
    public Set<ActiveElement> calculatePath(Network network,
                                            Long sourceId,
                                            Long destinationId) {
        Map<Long, Vertex> visitedVertexesById = new HashMap<>();
        Map<Long, Double> totalLatencyToVertexById = setInfiniteLatency(network);
        Vertex currentVertex = network.getActivePathElement(sourceId);
        Vertex destinationVertex = network.getActivePathElement(destinationId);

        while (!Objects.equals(currentVertex, destinationVertex)) {
            for (Pluggable pluggable : source.getPluggedElements()) {
                double currentLatency = currentVertex.getLatency() +
            }
        }
    }

    private Map<Long, Double> setInfiniteLatency(Network network) {
        Map<Long, Double> infiniteLatencyToElementById= new HashMap<>();

        for (ActiveElement element : network.getActivePathElements()) {
            infiniteLatencyToElementById.put(element.getId(), Double.MAX_VALUE);
        }

        return infiniteLatencyToElementById;
    }
}
