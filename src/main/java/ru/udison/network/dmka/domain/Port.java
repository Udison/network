package ru.udison.network.dmka.domain;

import lombok.Data;

@Data
public class Port implements BusinessEntity {

    private final long id;
    private Connective connective;

    public Port(Long id) {
        this.id = id;
    }
}
