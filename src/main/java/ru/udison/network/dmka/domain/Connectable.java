package ru.udison.network.dmka.domain;

import java.util.List;

/**
 * Interface provides the basic connectivity operations over the path elements
 * To connect such elements use {@code Pluggable.plugLeft} or {@code Pluggable.plugRight}. See {@link Pluggable}
 */
public interface Connectable extends Vertex {

    void connect(Connective connective);

    void disconnect(Connective connective);

    boolean connected(Connective connective);

    List<Connective> getConnectiveElements();
}
