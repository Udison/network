package ru.udison.network.dmka.domain;

public interface Chord {

    Vertex getStartVertex();

    Vertex getEndVertex();

    double getWeight();
}
