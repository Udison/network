package ru.udison.network.dmka.domain;

import lombok.Data;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Data
public class Cable extends PathElement implements Connective {

    private Connectable leftElement;
    private Connectable rightElement;

    public Cable(Long id) {
        super(id);
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void plugLeft(Connectable connectable) {
        if (this.leftElement != null) {
            throw new IllegalStateException(
                            String.format("This cable is already plugged into %s",
                            this.leftElement.getId()));
        }

        this.leftElement = connectable;
        plug(connectable);
    }

    @Override
    public void plugRight(Connectable connectable) {
        if (this.rightElement != null) {
            throw new IllegalStateException(
                            String.format("This cable is already plugged into %s",
                            this.rightElement.getId()));
        }

        this.rightElement = connectable;
        plug(connectable);
    }

    @Override
    public void unplugLeft() {
        this.leftElement = null;
    }

    @Override
    public void unplugRight() {
        this.rightElement = null;
    }

    @Override
    public boolean pluggedInto(Connectable connectable) {
        return  Objects.equals(leftElement, connectable) ||
                Objects.equals(rightElement, connectable);
    }

    @Override
    public List<Connectable> getConnectedElements() {
        return Arrays.asList(this.leftElement, this.rightElement);
    }

    private void plug(Connectable connectable) {
        if (!connectable.connected(this)) {
            connectable.connect(this);
        }
    }

    @Override
    public Vertex getStartVertex() {
        return leftElement;
    }

    @Override
    public Vertex getEndVertex() {
        return rightElement;
    }

    @Override
    public double getWeight() {
        return getLatency();
    }
}
