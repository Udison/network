package ru.udison.network.dmka.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.udison.network.dmka.Network;

/**
 * The root class which represents the path element of a network. See {@link Network}
 */
@Data
public abstract class PathElement implements BusinessEntity {

    private final long id;

    /**
     * All path elements have it's own latency (milliseconds)
     */
    private double latency;

    public PathElement(long id) {
        this.id = id;
    }

    public abstract boolean isActive();
}
