package ru.udison.network.dmka.domain;

import java.util.Set;

public interface Vertex extends BusinessEntity {

    Set<Vertex> getNeighbors();

    double getWeight();

    double getWeightToNeighbor(Vertex vertex);
}
