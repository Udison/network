package ru.udison.network.dmka.domain;

import java.util.*;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

public class Device extends PathElement implements Connectable {

    private final List<Port> ports = new ArrayList<>();
    private int availablePorts;

    public Device(long id, int portsCount) {
        super(id);
        createPorts(portsCount);
    }

    protected void createPorts(int portsCount) {
        for (int i = 0; i < portsCount; i++) {
            this.ports.add(new Port(1000L + i));
        }

        availablePorts = portsCount;
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void connect(Connective connective) {
        if (availablePorts == 0) {
            throw new IllegalStateException("There are no available ports!");
        }

        ports.get(--availablePorts).setConnective(connective);
    }

    @Override
    public void disconnect(Connective connective) {
        ports.stream()
                .filter(p -> Objects.equals(connective, p.getConnective()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("This PC is not connected to the pluggable with ID = %s", connective.getId())))
                .setConnective(null);
    }

    @Override
    public boolean connected(Connective connective) {
        return ports.stream()
                .anyMatch(p -> Objects.equals(connective, p.getConnective()));
    }

    @Override
    public List<Connective> getConnectiveElements() {
        return ports.stream()
                .map(Port::getConnective)
                .filter(Objects::nonNull)
                .collect(toList());
    }

    @Override
    public Set<Vertex> getNeighbors() {
        return ports.stream()
                .map(Port::getConnective)
                .filter(Objects::nonNull)
                .map(Pluggable::getConnectedElements)
                .flatMap(List::stream)
                .collect(toSet());
    }

    @Override
    public double getWeight() {
        return getLatency();
    }

    @Override
    public double getWeightToNeighbor(Vertex vertex) {
        Chord chordToDestinationVertex = ports.stream()
                .map(Port::getConnective)
                .filter(Objects::nonNull)
                .collect(toMap(identity(), Pluggable::getConnectedElements))
                .entrySet().stream()
                .filter(e -> checkChordGoesToDestination(e, vertex))
                .map(Map.Entry::getKey)
                .min(Comparator.comparingDouble(Chord::getWeight))
                .orElseThrow(() -> new IllegalArgumentException(String.format("There is no such neighbor with ID = %s!")));

        return this.getWeight() + chordToDestinationVertex.getWeight() + vertex.getWeight();
    }

    private boolean checkChordGoesToDestination(Map.Entry<Connective, List<Connectable>> entry, Vertex destination) {
        return entry.getValue().stream()
                .anyMatch(con -> Objects.equals(con.getId(), destination.getId()));
    }
}
