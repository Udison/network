package ru.udison.network.dmka.domain;

import java.util.List;

/**
 * Interface describes the basic operations over the path elements which can be plugged into {@link Connectable}
 * Use {@code Pluggable.plugLeft} or {@code Pluggable.plugRight} to connect {@link Connectable} path elements
 */
public interface Pluggable extends BusinessEntity {

    void plugLeft(Connectable connectable);

    void plugRight(Connectable connectable);

    void unplugLeft();

    void unplugRight();

    boolean pluggedInto(Connectable connectable);

    List<Connectable> getConnectedElements();
}
