package ru.udison.network.dmka;

public interface NetworkCreator {

    Network create();
}
