package ru.udison.network.dmka;

import ru.udison.network.dmka.domain.*;

import java.util.ArrayList;
import java.util.List;

public class DefaultNetworkCreator implements NetworkCreator {

    @Override
    public Network create() {
        Network network = new Network();

        List<Device> devices = createPCs();

        connectElements (
                devices
        );

        devices.forEach(network::addActivePathElement);
        switches.forEach(network::addActivePathElement);
        routers.forEach(network::addActivePathElement);

        return network;
    }


    protected List<Device> createPCs() {
        List<Device> devices = new ArrayList<>();

        devices.add(createPC(1, 3, Math.random()));
        devices.add(createPC(2, 3, Math.random()));
        devices.add(createPC(3, 3, Math.random()));

        return devices;
    }


    protected List<Switch> createSwitches() {
        List<Switch> switches = new ArrayList<>();

        switches.add(createSwitch(4, 10, Math.random()));
        switches.add(createSwitch(5, 10, Math.random()));
        switches.add(createSwitch(6, 10, Math.random()));

        return switches;
    }


    protected List<Router> createRouters() {
        List<Router> routers = new ArrayList<>();

        routers.add(createRouter(7, 10, Math.random()));
        routers.add(createRouter(8, 10, Math.random()));
        routers.add(createRouter(9, 10, Math.random()));

        return routers;
    }


    protected void connectElements(List<? extends Connectable> pcs,
                                   List<? extends Connectable> routers,
                                   List<? extends Connectable> switches) {
        Pluggable c1 = createCable(100, Math.random() * 10);
        Pluggable c2 = createCable(101, Math.random() * 10);

        c1.plugLeft(pcs.get(0));
        c1.plugRight(pcs.get(1));

        c2.plugLeft(pcs.get(1));
        c2.plugRight(pcs.get(2));

        Pluggable c3 = createCable(102, Math.random() * 10);
        Pluggable c4 = createCable(103, Math.random() * 10);

        c3.plugLeft(switches.get(0));
        c3.plugRight(switches.get(1));

        c4.plugLeft(switches.get(1));
        c4.plugRight(switches.get(2));

        Pluggable c5 = createCable(104, Math.random() * 10);
        Pluggable c6 = createCable(105, Math.random() * 10);

        c5.plugLeft(routers.get(0));
        c5.plugRight(routers.get(1));

        c6.plugLeft(routers.get(1));
        c6.plugRight(routers.get(2));

        Pluggable c7 = createCable(106, Math.random() * 10);
        Pluggable c8 = createCable(107, Math.random() * 10);
        Pluggable c9 = createCable(108, Math.random() * 10);

        c7.plugLeft(pcs.get(0));
        c7.plugRight(switches.get(0));

        c8.plugLeft(pcs.get(1));
        c8.plugRight(switches.get(1));

        c9.plugLeft(pcs.get(2));
        c9.plugRight(switches.get(2));

        Pluggable c10 = createCable(109, Math.random() * 10);
        Pluggable c11 = createCable(110, Math.random() * 10);
        Pluggable c12 = createCable(111, Math.random() * 10);

        c10.plugLeft(switches.get(0));
        c10.plugRight(routers.get(0));

        c11.plugLeft(switches.get(1));
        c11.plugRight(routers.get(1));

        c12.plugLeft(switches.get(2));
        c12.plugRight(routers.get(2));
    }


    private Device createPC(long id, int portsCount, double latency) {
        Device device = new Device(id, portsCount);
        device.setLatency(latency);

        return device;
    }


    private Switch createSwitch(long id, int portsCount, double latency) {
        Switch sw = new Switch(id, portsCount);
        sw.setLatency(latency);

        return sw;
    }


    private Router createRouter(long id, int portsCount, double latency) {
        Router router = new Router(id, portsCount);
        router.setLatency(latency);

        return router;
    }

    private Cable createCable(long id, double latency) {
        Cable cable = new Cable(id);
        cable.setLatency(latency);

        return cable;
    }
}
