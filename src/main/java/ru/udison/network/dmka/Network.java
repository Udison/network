package ru.udison.network.dmka;

import lombok.Data;
import ru.udison.network.task.ActiveElement;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Data
public class Network {

    private final Map<Long, ActiveElement> activePathElements = new HashMap<>();

    public void addActivePathElement(ActiveElement activeElement) {
        activePathElements.putIfAbsent(activeElement.getId(), activeElement);
    }


    public void removeActivePathElement(Long id) {
        activePathElements.remove(id);
    }


    public ActiveElement getActivePathElement(Long id) {
        return activePathElements.get(id);
    }


    public Set<ActiveElement> getActivePathElements() {
        return new HashSet<>(activePathElements.values());
    }
}
