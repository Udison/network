package ru.udison.network.dmka;

public class Application {

    public static void main(String[] args) {
        NetworkCreator networkCreator = new DefaultNetworkCreator();
        Network network = networkCreator.create();

        System.out.println(network);
    }
}
