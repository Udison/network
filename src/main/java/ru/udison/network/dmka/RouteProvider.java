package ru.udison.network.dmka;

import ru.udison.network.task.ActiveElement;

import java.util.Set;

/**
 * Interface provides ability to find path the most optimal path in the network
 * @param <K> the key type of the elements
 * @param <V> path element in the network which belongs to the calculated path
 */
public interface RouteProvider<K, V extends ActiveElement> {

    Set<V> calculatePath(Network network, K from, K to);
}
